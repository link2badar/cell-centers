<?php

Route::group(['middlewareGroups' => ['web']], function() {
    Route::auth();
    
    /* Token Validation */
	Route::post('/verifyToken/validate-token' , 'LoginController@validateToken');


    /* Appointment  Routes  */
    Route::get('/calendar'                  ,   'AppointmentController@index');
    Route::get('/appointment/add'           ,   'AppointmentController@add');
    Route::post('/appointment/add'          ,   'AppointmentController@add');


    /* Physicians  Routes  */
    Route::get('/locations'                 ,   'LocationController@index');
    Route::get('/location/add'              ,   'LocationController@add');
    Route::post('/location/add'             ,   'LocationController@add');
    Route::get('location/update/{id?}'      ,   'LocationController@update');
    Route::post('location/update/{id?}'     ,   'LocationController@update');
    Route::get('location/delete/{id}'       ,   'LocationController@delete');



    /* Seminar Routes  */
    Route::get('/seminars'              ,   'SeminarController@index');
    Route::get('/seminar/add'           ,   'SeminarController@add');
    Route::post('/seminar/add'          ,   [
                                                'as'    =>  'clinicadd',
                                                'uses'  =>  'SeminarController@add'
                                            ]);
    Route::get('/seminar/detail/{id?}'  ,   'SeminarController@viewDetail');
    Route::get('seminar/update/{id}'    ,   'SeminarController@update');
    Route::post('seminar/update/'       ,   [
                                                'as'    =>  'updateclinic',
                                                'uses'  =>  'SeminarController@update'
                                             ]);
    Route::get('seminar/delete/{id}'    ,   'SeminarController@delete');

    Route::get('seminar/admin-modal'    ,   'SeminarController@seminarAdminModal');
    Route::post('seminar/add-admin'     ,   'SeminarController@addSeminarAdmin');
    Route::post('seminar/update-admin'  ,   'SeminarController@updateSeminarAdmin');
    Route::get('seminar/delete-admin/{id?}','SeminarController@deleteSeminarAdmin');

    Route::get('seminar/add-registrant/{id?}'   ,   'SeminarController@seminarAddRegistrant');
    Route::post('seminar/add-registrant/'       ,   [
                                                        'as'    =>  'RegistrantAdd',
                                                        'uses'  =>  'SeminarController@seminarAddRegistrant'
                                                    ]);
    Route::get('seminar/delete-registrant/{id}' ,   'SeminarController@deleteSeminarRegistrant');
    Route::post('seminar/registrant-attendance' ,   'SeminarController@markRegistrantAttendance');

    Route::get('seminar/edit-registrant/{id}'   ,   'SeminarController@viewEditRegistrant');
    Route::post('seminar/update-registrant'     ,   'SeminarController@updateRegistrant');
    
    Route::get('patient/log/{id}'               ,   'SeminarController@viewPatientFroms');
    Route::get('patient/forms/{id}'             ,   'SeminarController@viewPatientFroms');
    Route::get('patient/seminar-notes/{id}'     ,   'SeminarController@viewSeminarNotes');
    Route::get('patient/consultation-notes/{id}',   'SeminarController@viewConsultationNotes');
    Route::get('patient/exam-notes/{id}'        ,   'SeminarController@viewExamNotes');
    Route::get('patient/treatment-notes/{id}'   ,   'SeminarController@viewTreatmentNotes');

    Route::get('patient/appointments/{id}'      ,   'SeminarController@viewAppointments');
    Route::get('patient/invoices/{id}'          ,   'SeminarController@viewInvoices');
    Route::get('patient/seminars/{id}'          ,   'SeminarController@viewSeminars');

    Route::get('seminar/create-note'            ,   'SeminarController@seminarCreateNoteModal');
    Route::post('seminar/create-note'           ,   'SeminarController@seminarCreateNote');
    Route::get('seminar/full-note'              ,   'SeminarController@seminarReadFullNote');


    /* Clinics Routes  */
    Route::get('/clinics'               ,   'ClinicController@index');
    Route::get('/clinic/add'            ,   'ClinicController@add');
    Route::post('/clinic/add'           ,   [
                                                'as'    =>  'clinicadd',
                                                'uses'  =>  'ClinicController@add'
                                            ]);
    Route::get('clinic/update/{id?}'    ,   'clinicController@update');
    Route::post('clinic/update/{id?}'   ,    [
                                                'as'    =>  'updateclinic',
                                                'uses'  =>  'clinicController@update'
                                        ]   );
    Route::get('clinic/delete/{id}'     ,   'clinicController@delete');


    /* Physicians  Routes  */
    Route::get('/physicians'                ,   'PhysicianController@index');
    Route::get('/physician/add'             ,   'PhysicianController@add');
    Route::post('/physician/add'            ,   [
                                                    'as'    =>  'clinicadd',
                                                    'uses'  =>  'PhysicianController@add'
                                                ]);
    Route::get('physician/update/{id?}'     ,   'PhysicianController@update');
    Route::post('physician/update/{id?}'    ,    [
                                                    'as'    =>  'updateclinic',
                                                    'uses'  =>  'PhysicianController@update'
                                                ]);
    Route::get('physician/delete/{id}'      ,   'PhysicianController@delete');


    /* Managers Routes  */
    Route::get('/managers'                 ,   'ManagerController@index');
    Route::get('/manager/add'              ,   'ManagerController@add');
    Route::post('/manager/add'             ,   [
                                                     'as'    =>  'clinicadd',
                                                    'uses'  =>  'ManagerController@add'
                                                ]);
    Route::get('manager/update/{id?}'      ,   'ManagerController@update');
    Route::post('manager/update/{id?}'     ,    [
                                                    'as'    =>  'updateclinic',
                                                    'uses'  =>  'ManagerController@update'
                                                ]);
    Route::get('manager/delete/{id}'       ,   'ManagerController@delete');

    /* PATIENT ROUTES */
    Route::get("/seminar-users"             ,   'SeminarUserController@index');
    Route::get('/seminar-user/add'          ,   'SeminarUserController@add');
    Route::post('/seminar-user/add'         ,   [
                                                    'as' => 'patientadd',
                                                    'uses' => 'SeminarUserController@add'
                                                ]);
    Route::get('seminar-user/delete/{id}'   ,   'SeminarUserController@delete');
    Route::get('seminar-user/update/{id?}'  ,   'SeminarUserController@update');
    Route::post('seminar-user/update/{id?}' ,   [
                                                    'as' => 'updatepatient',
                                                    'uses' => 'SeminarUserController@update'
                                                ]); 

    /* Staff Routes*/
    Route::get('/staff'                     ,   'StaffController@index');
    Route::get('/staff/add'                 ,   'StaffController@add');
    Route::post('/staff/add'                ,   [
                                                    'as'    =>  'clinicadd',
                                                    'uses'  =>  'StaffController@add'
                                                ]);
    Route::get('staff/update/{id?}'         ,   'StaffController@update');
    Route::post('staff/update/{id?}'        ,    [
                                                    'as'    =>  'updateclinic',
                                                    'uses'  =>  'StaffController@update'
                                                ]);
    Route::get('staff/delete/{id}'          ,   'StaffController@delete');

    /* PATIENT ROUTES */
    Route::get("/patients"              ,   'PatientController@index');
    Route::get('/patient/add'           ,   'PatientController@add');
    Route::post('/patient/add'          ,   [
                                                'as'    =>  'patientadd',
                                                'uses'  =>  'PatientController@add'
                                            ]);
    Route::get('patient/delete/{id}'    ,   'PatientController@delete');
    Route::get('patient/update/{id?}'   ,   'PatientController@update');
    Route::post('patient/update/{id?}'  ,   [
                                                'as'    =>  'updatepatient',
                                                'uses'  =>  'PatientController@update'
                                        ]); 
    Route::post('/patient/filter-provider', 'PatientController@filterProvider');

    //CONTACT ROUTES
    Route::get("/contacts"              ,   'ContactController@index');
    Route::get('/contact/add'           ,   'ContactController@add');
    Route::post('/contact/add'          ,   [
                                            'as' => 'patientadd',
                                            'uses' => 'ContactController@add'
                                        ]);
    Route::get('/contact/delete/{id}'   ,   'ContactController@delete');
    Route::get('/contact/update/{id?}'  ,   'ContactController@update');
    Route::post('/contact/update/{id?}' ,   [
                                                'as' => 'updatepatient',
                                                'uses' => 'ContactController@update'
                                            ]);  
    
    /* DASHBAORD ROUTES */
    Route::get('/'              ,   'DashboardController@index');
    Route::get('/dashboard'     ,   'DashboardController@index');


});

