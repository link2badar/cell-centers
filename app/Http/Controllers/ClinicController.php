<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Clinics;
use Auth,URL,Session,Redirect,DB,Validator;

class ClinicController  extends Controller{

	private $storage    =  "media/";
    private $plural     =  "Clinics";    
	private $module     =  "Clinic";
    private $view       =  "clinics/";     
	
    public function __construct()	{
        $this->contants   =   config('constants.appmanangement');
	}
	
    public function index(Request $request)	{
		$data = array(
            "page_title"    =>  $this->module." Management | View All ".$this->plural,
            "page_heading"  =>  $this->module." Management | View All ".$this->plural,
            "module"        =>  $this->module,
            "storage"       =>  $this->storage,
            "breadcrumbs"   =>  array("dashboard" => "Home", "#"  => ucfirst($this->plural)." List")
        );
        $data['list']   =   Clinics::all()->toArray();
		return view($this->view.'.list',$data);
	}

    public function add(Request $request) {
        if($request->method() == 'POST'){ 
            $data   =   $request->all();
            $validator  =   Validator::make($data,[
                'clinic_name'       =>  'required',
                'clinic_phone_no'   =>  'required',
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            if ($request->hasFile('clinic_image')) {
                $file            =  $request->file('clinic_image');
                $destinationPath =  base_path() . '/public/clinics_imgs/';
                $filename        =  $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $data['image']   =  $filename;
            }
            unset($data['_token'],$data['clinic_image']);
            $clinic     =   new Clinics();
            $clinic->insert($data);
            return redirect('clinics')->with('message', $this->module.' has been sucessfully added !');
        }
        $data = array(
            "page_title"    =>  "Add New ". $this->module,
            "page_heading"  =>  "Add New ". $this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('clinics') =>  ucfirst($this->plural)." List" , '#' =>'Add New '.ucfirst($this->module)),
        );
        return view($this->view.'.add-view' , $data);
    }

    public function update(Request $request,$id = NULL) {
        if($request->method() == 'POST'){ 
            $data       =   $request->all();
            $validator  =   Validator::make($data,[
                'clinic_name'       =>  'required',
                'clinic_phone_no'   =>  'required',
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            if ($request->hasFile('clinic_image')) {
                $file            =  $request->file('clinic_image');
                $destinationPath =  base_path() . '/public/clinics_imgs/';
                $filename        =  $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $data['image']   =  $filename;
            }
            unset($data['_token'],$data['clinic_image']);
            $clinic  =   Clinics::find($id);
            $clinic->update($data);
            return redirect('clinic/update/'.$id)->with('message', 'Contact sucessfully added');
        }
        $data = array(
            "page_title"    =>  "Edit ".$this->module,
            "page_heading"  =>  "Edit ".$this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('clinics') =>  ucfirst($this->plural)." List" , '#' =>'Edit '.ucfirst($this->module)),
        );
        $data['clinic']     =   Clinics::find($id)->toArray();
        return view($this->view.'.edit-view', $data);
    }

    public function delete($id) {
        $employee   =  Clinics::find($id);
        $employee->delete();
        $response = array('flag' => true, 'msg' => $this->module . ' has been Deactivated');
        echo json_encode($response);
    }
}
