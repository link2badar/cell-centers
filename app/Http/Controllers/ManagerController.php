<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Managers;
use Auth,URL,Session,Redirect,DB,Validator;

class ManagerController   extends Controller{

    private $plural     =  "Managers";    
	private $module     =  "Manager";
    private $view       =  "managers/";     
	
    public function __construct()	{
        $this->contants   =   config('constants.appmanangement');
	}
	
    public function index(Request $request)	{
		$data = array(
            "page_title"    =>  $this->module." Management | View All ".$this->plural,
            "page_heading"  =>  $this->module." Management | View All ".$this->plural,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", "#"  => ucfirst($this->plural)." List")
        );
        $data['list']       =   Managers::all()->toArray();
		return view($this->view.'.list',$data);
	}

    public function add(Request $request) {
        if($request->method() == "POST"){
            $data       =   $request->all();
            $validator  =   Validator::make($data,[
                'first_name'  =>  'required',
                'last_name'   =>  'required',
                'email'       =>  'unique:managers|required',
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            if ($request->hasFile('manager_image')) {
                $file            =  $request->file('manager_image');
                $destinationPath =  base_path() . '/public/managers_imgs/';
                $filename        =  $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $data['image']   =  $filename;
            }
            unset($data['_token'],$data['manager_image']);
            $physician     =   new Managers();
            $physician->insert($data);
            return redirect('managers')->with('message', $this->module.' has been sucessfully added !');
        }
        $data = array(
            "page_title"    =>  "Add New ". $this->module,
            "page_heading"  =>  "Add New ". $this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('managers') =>  ucfirst($this->plural)." List" , '#' =>'Add New '.ucfirst($this->module)),
        );
        return view($this->view.'.add-view' , $data);
    }

    public function update(Request $request,$id = NULL) {
        if($request->method() == "POST"){
            $data       =   $request->all();
            $validator  =   Validator::make($data,[
                'first_name'  =>  'required',
                'last_name'   =>  'required',
                "email"       =>  "required|unique:managers,email,$id,manager_id",
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            if ($request->hasFile('manager_image')) {
                $file            =  $request->file('manager_image');
                $destinationPath =  base_path() . '/public/managers_imgs/';
                $filename        =  $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $data['image']   =  $filename;
            }
            unset($data['_token'],$data['manager_image']);
            $clinic  =   Managers::find($id);
            $clinic->update($data);
            return redirect('manager/update/'.$id)->with('message', 'Manager sucessfully added');
        }
        $data = array(
            "page_title"    =>  "Edit ".$this->module,
            "page_heading"  =>  "Edit ".$this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('managers') =>  ucfirst($this->plural)." List" , '#' =>'Edit '.ucfirst($this->module)),
        );
        $data['manager']  =   Managers::find($id)->toArray();
        return view($this->view.'.edit-view', $data);
    }

    public function delete($id) {
        $manager   =  Managers::find($id);
        $manager->delete();
        $response = array('flag' => true, 'msg' => $this->module . ' has been Deactivated');
        echo json_encode($response);
    }
}
