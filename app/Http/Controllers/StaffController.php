<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Staff;
use Auth,URL,Session,Redirect,Validator;
Use App\User;

class StaffController   extends Controller{

    private $plural     =  "Staff Members";    
    private $module     =  "Staff Member";
    private $view       =  "staff/";     
    
    public function __construct()   {
        $this->contants   =   config('constants.appmanangement');
    }
    
    public function index(Request $request) {
        $data = array(
            "page_title"    =>  $this->module." Management | View All ".$this->plural,
            "page_heading"  =>  $this->module." Management | View All ".$this->plural,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", "#"  => ucfirst($this->plural)." List")
        );
        $data['list']       =   User::where(array('role' => '3'))->get();
        return view($this->view.'.list',$data);
    }

    public function add(Request $request) {
        if($request->has('name')){
            $data       =   $request->all();
            $validator  =   Validator::make($request->all(),[
                'email'         =>  'unique:users',
                'password'      =>  'required|confirmed|min:6',
                'password'      =>  'required|confirmed|min:6',
                'positions'     =>  'required'
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            $data['positions']  =   implode(',',$data['positions']);
            unset($data['_token'],$data['password_confirmation']);
            $data['password']   =   bcrypt($data['password']);
            $data['role']       =   3;
            $data['location_id']=   Auth::user()->location_id;
            $staff     =   new User();
            $staff->insert($data);
            return redirect('staff')->with('message', $this->module.' has been sucessfully added !');
        }
        $data = array(
            "page_title"    =>  "Add ". $this->module,
            "page_heading"  =>  "Add ". $this->module." Member",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('staff') =>  ucfirst($this->plural)." List" , '#' =>'Add '.ucfirst($this->module)),
        );
        return view($this->view.'.add-view' , $data);
    }

    public function update(Request $request,$id = NULL) {
        if($request->method() == "POST"){
            $data       =   $request->all();
            $validator  =   Validator::make($data,[
                'name'     =>   'required',
                "email"    =>   "required|unique:Users,email,$id,id",
                'password' =>   'confirmed|min:6',
                'positions'=>   'required'
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            unset($data['_token'],$data['password_confirmation']);
            $data['positions']   =    implode(",", $data['positions']);
            $staff  =   User::find($id);
            if ($data['password'] == '') {
                $data['password'] = $staff['password'];
            }

            $staff->update($data);
            return redirect('staff')->with('message', 'Contact sucessfully added');
        }
        $data = array(
            "page_title"    =>  "Edit ".$this->module,
            "page_heading"  =>  "Edit ".$this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('managers') =>  ucfirst($this->plural)." List" , '#' =>'Edit '.ucfirst($this->module)),
        );
        $data['staff']      =   User::find($id)->toArray();
        return view($this->view.'.edit-view', $data);
    }

    public function delete($id) {
        $manager   =  User::find($id);
        $manager->delete();
        $response = array('flag' => true, 'msg' => $this->module . ' has been Deactivated');
        echo json_encode($response);
    }
}
