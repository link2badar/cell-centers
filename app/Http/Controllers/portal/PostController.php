<?php

namespace App\Http\Controllers\appmanangement;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\appmanangement\Apps;
use App\Models\appmanangement\Stores;
use App\Models\appmanangement\Posts;
use App\Models\Utility;
use App\Classes\ImageUtility;
use Hash;
use Auth,URL,Session,Redirect;
use DB;
class PostController extends Controller
{
	private $storege = "mobileapp/";
	private $module = "";
    private $thumbs = [200,400];
	public function __construct()	{
		$this->module = config('constants.appmanangement.module');
	}
	
	
    public function index(Request $request,$id)	{
                
		$data = array(
            "page_title"   => "App Management | View All Posts",
            "page_heading" => "App Management | View All Posts",
            "module" => $this->module,
            "storege"=>$this->storege,
            "breadcrumbs"  => array("dashboard" => "Home", "#" => $this->module),
        );

        $query  =   Posts::where('parent_id','=',$id);
        $data['list']   =   $query->paginate(50)->toArray();
        $data['parent_id']   =   $id;
 
        
		return view($this->module.'.post.view', $data);
	}

    public function add(Request $request,$parent_id)   {
        $data = $request->all();
        

        if(isset($data['title'])){ 
            $tags = $data['tags'];
            $data['parent_id'] = $parent_id;
            if(isset($data['tags'])) unset($data['tags']);           
            $is_save                =   Posts::where('title','=',
                                                $request->name)
                                                ->where('parent_id','=',$parent_id)
                                                ->count();
            if($is_save > 0)    {
                return redirect($this->module.'/post/add/'.$parent_id)->with('message', 'Post with this title alrady exist.');
            }

            if($data['type'] == 0){
                if(isset($data['multiple_image'])) unset($data['multiple_image']);
                if(isset($data['video_url'])) unset($data['video_url']);
            
                if($data['title'] == ''){
                    return redirect($this->module.'/post/add/'.$parent_id)->with('message', 'Post title is required.');
                }

                $app = new Posts();
                $app->create($data);
                return redirect($this->module.'/post/'.$parent_id)->with('message', 'Post is added sucessfully!');
            }else if($data['type'] == 1) {
                
                if(isset($data['multiple_image'])) unset($data['multiple_image']);
                if(isset($data['video_url'])) unset($data['video_url']);

                if(!$request->file('single_image')){
                    return redirect($this->module.'/post/add/'.$parent_id)->with('message', 'Post Image is required.');
                }
                $image = new ImageUtility($this->storege);
                
                $data['image_url'] = $image->uploadImage($this,$request,'single_image',1);
                foreach ($this->thumbs as $key => $value) {
                    $image->createThumbnail($data['image_url'],$value,$value,storage_path($this->storege),storage_path($this->storege));
                }
                if(isset($data['single_image'])) unset($data['single_image']);                    
                $app = new Posts();
                $app->create($data);
                return redirect($this->module.'/post/'.$parent_id)->with('message', 'Post is added sucessfully!');
            }else if($data['type'] == 2) {
                if(!$request->file('multiple_image')){
                    return redirect($this->module.'/post/add/'.$parent_id)->with('message', 'Post Images is required.');
                }
                $images = [];
                
                $image = new ImageUtility($this->storege);
                $images = $image->uploadMultipleImages($request,$this,'multiple_image');
                foreach ($images as $key => $value) {
                    foreach ($this->thumbs as $ikey => $ivalue) {
                        $image->createThumbnail($value,$ivalue,$ivalue,storage_path($this->storege),storage_path($this->storege));
                    }
                }
                if(isset($data['multiple_image'])) unset($data['multiple_image']);
                if(isset($data['video_url'])) unset($data['video_url']);
                $data['image_url'] = json_encode($images);
                $data['multiple'] = 1;
                $app = new Posts();
                $app->create($data);
                return redirect($this->module.'/post/'.$parent_id)->with('message', 'Post is added sucessfully!');
            }else if($data['type'] == 3) {
                if(isset($data['multiple_image'])) unset($data['multiple_image']);
              

                if(!$request->file('image_preview')){
                    return redirect($this->module.'/post/add/'.$parent_id)->with('message', 'Post Image is required.');
                }
                $image = new ImageUtility($this->storege);
                
                $data['image_url'] = $image->uploadImage($this,$request,'image_preview',1);
                foreach ($this->thumbs as $key => $value) {
                    $image->createThumbnail($data['image_url'],$value,$value,storage_path($this->storege),storage_path($this->storege));
                }
                if(isset($data['image_preview'])) unset($data['image_preview']);                    
                $app = new Posts();
                $app->create($data);
                return redirect($this->module.'/post/'.$parent_id)->with('message', 'Post is added sucessfully!');
                
            }
            
        }

        $data = array(
            "page_title"   => "App Management | Create Post",
            "page_heading" => "App Management | Create Post",
            "module" => $this->module,
            "breadcrumbs"  => array("dashboard" => "Home", "#" => $this->module),
        );
        $data['parent_id'] =   $parent_id;
        
        return view($this->module.'.post.addview', $data);
    }

    public function delete($id) {
        $app               = Apps::find($id)->toArray();
        $unlink = storage_path($this->storege).$app['app_img'];
        foreach ($this->thumbs as $key => $value) {
            $value = storage_path($this->storege.$value.'-'.$app['app_img']);
         
            if(file_exists($value)){
                @unlink($value);
            }
            $value = storage_path($this->storege.$app['app_img']);
            if(file_exists($value)){
                @unlink($value);
            }
            $value = storage_path();
        }        
        Apps::destroy($id);
        $response = array('flag'=>true,'msg'=>'Category has been deleted.');
        echo json_encode($response); return;
    }
    public function update($id = NULL,Request $request) {
         $app  = Apps::find($id)->toArray();
        //print_r($request->all()); die();
        if($request->input('name')){
            
            
            $is_save                =   Apps::where('name','=',$request->name)
                                                ->where('app_id','!=',$id)
                                                ->count();
            if($is_save > 0)    {
       
                return redirect($this->module.'/category/update/'.$id)->with('message', 'This category already exist');
            }
            $data = $request->all();
            $image = new ImageUtility($this->storege);
            if($request->file('userfile')){
                
                $data['app_img'] = $image->uploadImage($this,$request,'userfile',1);
                foreach ($this->thumbs as $key => $value) {
                    $image->createThumbnail($data['app_img'],$value,$value,storage_path($this->storege),storage_path($this->storege));
                }
                foreach ($this->thumbs as $key => $value) {
                    $value = storage_path($this->storege.$value.'-'.$app['app_img']);
                 
                    if(file_exists($value)){
                        @unlink($value);
                    }
                    $value = storage_path($this->storege.$app['app_img']);
                    if(file_exists($value)){
                        @unlink($value);
                    }
                    $value = storage_path();
                }  

            }
            $app               = Apps::find($id);
            $app->update($data);
            return redirect($this->module.'/category/'.$app['parent_id'])->with('message', 'Category is updated sucessfully!');
        }
        $data = array(
            "page_title"   => "Twitter Assistance | Edit App",
            "page_heading" => "Twitter Assistance | Edit App",
             "module" => $this->module,
             "storege"=>$this->storege,
            "breadcrumbs"  => array("dashboard" => "Home", "#" => "category"),
        );
        $data['list']       =   Apps::get();
        $data['data_row']   =   Apps::find($id);
        $data['stores']     =   Stores::get()->toArray();
        return view($this->module.'.category.editview', $data);
    }
    function postTags($post_id,$tags,$action = 'add'){

    }
	
}
