<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\appmanangement\Apps;
use App\Models\Patients;
use App\Models\Contacts;

use App\Models\Utility;
use Hash;
use Auth,URL,Session,Redirect;
use DB;
class ContactController extends Controller
{
	private $storege = "media/";
    

	private $module = "";
    private $contants;
	public function __construct()	{
		$this->module = config('constants.appmanangement.module');
        $this->contants = config('constants.appmanangement');
	}
	
	
    public function index(Request $request)	{
		$data = array(
            "page_title"   => "Contact Management | View All Contact",
            "page_heading" => "Contact Management | View All Contact",
            "module" => $this->module,
            "storege"=>$this->storege,
            "breadcrumbs"  => array("dashboard" => "Home", $this->module => ucfirst($this->module),'#'=>"Contact"),
        );

        $data['list']   =   Contacts::paginate(50)->toArray();
  
        
		return view('contact.view', $data);
	}
    public function add(Request $request)   {

        if($request->input('first_name')){
            
            
            $data = $request->all();
            $Contacts         = new Contacts;
            
            $Contacts->insert($data);
            return redirect('contacts')->with('message', 'Contact sucessfully added');
        }

        $data = array(
            "page_title"   => "Add Contact",
            "page_heading" => "Add Contact",
            "module" => $this->module,
            "breadcrumbs"  => array("dashboard" => "Home", url('contacts') => "Contact List",'#'=>'Add Contact'),
        );

        return view('contact.addview', $data);
    }

    public function delete($id) {
        $Stores   = new Contacts;
        $Stores->find($id);
        Contacts::destroy($id);
        $response = array('flag'=>true,'msg'=>'Contacts has been deleted.');
        echo json_encode($response); return;
    }
    public function update(Request $request,$id = NULL) {
        //print_r($request->all()); die();

        if($request->input('first_name')){

            
            
            $data = $request->all();
            if(isset($data['_token'])) unset($data['_token']);

        
            $store               = Contacts::find($id);
            $store->update($data);
            return redirect('contact/update/'.$id)->with('message', 'Contact sucessfully added');

            
        }
        $data = array(
            "page_title"   => "Edit Patients",
            "page_heading" => "Edit Patients",
            "breadcrumbs"  => array("dashboard" => "Home", url('patients') => "Patient List",'#'=>'Add Patient'),
        );
        $data['data_row']   =   Contacts::find($id)->toArray();
        return view('contact.editview', $data);
    }
	
}
