<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Physicians;
use App\User;
use App\Models\Locations;
use Auth,URL,Session,Redirect,DB,Validator;

class PhysicianController extends Controller{

    private $plural     =  "Physicians";    
	private $module     =  "Physician";
    private $view       =  "physicians/";     
	
    public function __construct()	{
	}
	
    public function index(Request $request)	{
		$data = array(
            "page_title"    =>  $this->module." Management | View All ".$this->plural,
            "page_heading"  =>  $this->module." Management | View All ".$this->plural,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", "#"  => ucfirst($this->plural)." List")
        );
        $data['list']       =   User::where('role', '2')->get();
		return view($this->view.'.list',$data);
	}

    public function add(Request $request) {
        if($request->method() == "POST"){
            $data       =   $request->all();
            $validator  =   Validator::make($data,[
                'name'     =>  'required',
                "email"    =>  "required|unique:users",
                'password' => 'required|confirmed|min:6',
                'location_id'=> 'required'
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            $physician     =   new User();
            unset($data['_token'],$data['password_confirmation']);
            $data['password']=      bcrypt($data['password']);
            $data['role']  =    2;
            $data['location_id']   =    implode(",", $data['location_id']);
            $physician->insert($data);
            return redirect('physicians')->with('message', $this->module.' has been sucessfully added !');
        }
        $data = array(
            "page_title"    =>  "Add New ". $this->module,
            "page_heading"  =>  "Add New ". $this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('physicians') =>  ucfirst($this->plural)." List" , '#' =>'Add New '.ucfirst($this->module)),
        );
        $data['allLocation']   =   Locations::all();
        return view($this->view.'.add-view' , $data);
    }

    public function update(Request $request,$id = NULL) {
        if($request->method() == "POST"){
            $data       =   $request->all();
            $validator  =   Validator::make($data,[
                'name'  =>  'required',
                "email"       =>  "required|unique:Users,email,$id,id",
                'password' => 'confirmed|min:6',
                'location_id'=> 'required'
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            unset($data['_token'],$data['password_confirmation']);
            $data['location_id']   =    implode(",", $data['location_id']);
            $physician  =   User::find($id);
            if ($data['password'] == '') {
                $data['password'] = $physician['password'];
            }

            $physician->update($data);
            return redirect('physicians')->with('message', 'Contact sucessfully added');
        }
        $data = array(
            "page_title"    =>  "Edit ".$this->module,
            "page_heading"  =>  "Edit ".$this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('physicians') =>  ucfirst($this->plural)." List" , '#' =>'Edit '.ucfirst($this->module)),
        );
        $data['physician']      =   User::find($id)->toArray();
        $data['allLocation']    =   Locations::all();
        return view($this->view.'.edit-view', $data);
    }

    public function delete($id) {
        $physician   =  User::find($id);
        $physician->delete();
        $response = array('flag' => true, 'msg' => $this->module . ' has been Deactivated');
        echo json_encode($response);
    }

}
