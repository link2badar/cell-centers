<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Patients;
use App\Models\Clinics;
use App\Models\Locations;
use App\Models\Managers;
use App\Models\Physicians;
use App\User;
use Auth,URL,Session,Redirect,Validator,DB;

class PatientController extends Controller{
	private $module    =   "Patient";
    private $view      =   "patients";      

	public function __construct()	{
	}
	
    public function index(Request $request)	{
		$data = array(
            "page_title"    =>  "Patient Management | View All Patient",
            "page_heading"  =>  "Patient Management | View All Patient",
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", $this->module => ucfirst($this->module),'#'=>"Patient"),
        );
        $data['list']   =   Patients::paginate(50)->toArray();  
		return view($this->view.'.list', $data);
	}

    public function add(Request $request)   {
        if($request->method() == 'POST'){ 
            $data       =   $request->all();
            $validator  =   Validator::make($data,[
                'first_name'    =>  'required',
                'last_name'     =>  'required',
                'email'         =>  'unique:patients|required',
                'haboutus'      =>  'required',
                'phone_number'  =>  'required',
                'preferred_clinic'=>'required'
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            if(!empty($data['primary_concern'])){
                $data['primary_concern']    =   implode(',',$data['primary_concern']);  
            }
            unset($data['_token']);
            $Patients   =   new Patients;
            $Patients->insert($data);
            return redirect('patients')->with('message', 'Patient sucessfully added');
        }
        $data = array(
            "page_title"   =>   $this->module." Management | Add ".$this->module,
            "page_heading" =>   $this->module." Management | Add ".$this->module,
            "module"       =>   $this->module,
            "breadcrumbs"  =>   array("dashboard" => "Home", url('patients') => "Patients List",'#'=>'Add Patient'),
        );
        $userClinics = explode(",",Auth::user()->location_id);
        $data['clinics']    =   Locations::whereIn('loc_id', $userClinics)->get();
        return view($this->view.'.add-view', $data);
    }

    public function filterProvider(Request $request) {
        $location_id        =   $request->all();
        $location_id        =   $location_id['param'];
        $result['manager']  =   User::select('name', 'id')->whereRaw("FIND_IN_SET('".$location_id."',location_id)")->whereRaw('FIND_IN_SET(3,positions)')->where('role', '3')->distinct()->get(); 
        $result['physician']=   User::select('name', 'id')->whereRaw("FIND_IN_SET('".$location_id."',location_id)")->where('role', '2')->distinct()->get(); 
        echo json_encode($result);
    }
    public function delete($id) {
        $patient   = new Patients;
        $patient->find($id);
        Patients::destroy($id);
        $response = array('flag'=>true,'msg'=>'Patient has been deleted.');
        echo json_encode($response); return;
    }

    public function update(Request $request,$id = NULL) {
        if($request->method() == 'POST'){ 
            $data   =   $request->all();
            $id     =   $data['pa_id'];
            $validator  =   Validator::make($data,[
                "first_name"    =>  "required",
                "last_name"     =>  "required",
                "email"         =>  "required|unique:patients,email,$id,pa_id",
                "haboutus"      =>  "required",
                "phone_number"  =>  "required",
            ]);
            if( $validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            if(!empty($data['primary_concern'])){
                $data['primary_concern']    =   implode(',',$data['primary_concern']);  
            }
            if(isset($data['_token'])) unset($data['_token']);
            $patient  = Patients::find($id);
            $patient->update($data);
            return redirect('patient/update/'.$id)->with('message', 'Patient updated sucessfully!');
            
        }
        $data = array(
            "page_title"   =>   "Edit Patient",
            "page_heading" =>   "Edit Patient",
            "module"       =>   $this->module,
            "breadcrumbs"  =>   array("dashboard" => "Home", url('patients') => "Patient List",'#'=>'Edit Patient'),
        );
        $userClinics        =   explode(",",Auth::user()->location_id);
        $data['clinics']    =   Locations::whereIn('loc_id', $userClinics)->get();
        $data['patient']    =   Patients::find($id)->toArray();
        return view($this->view.'.edit-view',$data);
    }
	
}
