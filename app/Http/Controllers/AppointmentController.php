<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Physicians;
use App\Models\Patients;
use Auth,URL,Session,Redirect;
use DB;

class AppointmentController extends Controller{

    private $plural     =  "Appointments";    
	private $module     =  "Appointment";
    private $view       =  "appointments/";     
	
    public function __construct()	{
        $this->contants   =   config('constants.appmanangement');
	}
	
    public function index(Request $request)	{
		$data = array(
            "page_title"    =>  'Calendar',
            "page_heading"  =>  'Calendar',
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", "#"  => "Calendar")
        );
        $data['list']       =   [];//Physicians::all()->toArray();
		return view($this->view.'.calendar',$data);
	}

    public function add(Request $request) {
        /*if($request->has('first_name')){
            $data       =   $request->all();
            if ($request->hasFile('physician_image')) {
                $file            =  $request->file('physician_image');
                $destinationPath =  base_path() . '/public/physicians_imgs/';
                $filename        =  $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $data['image']   =  $filename;
            }
            unset($data['_token'],$data['physician_image']);
            $physician     =   new Physicians();
            $physician->insert($data);
            return redirect('physicians')->with('message', $this->module.' has been sucessfully added !');
        }*/
        $data = array(
            "page_title"    =>  "Add New ". $this->module,
            "page_heading"  =>  "Add New ". $this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('calendar') => " Calendar" , '#' =>'Add New '.ucfirst($this->module)),
        );
        $data['physicians'] =   Physicians::all()->toArray();
        $data['patients']   =   Patients::all()->toArray();
        return view($this->view.'.add-view' , $data);
    }

    public function update(Request $request,$id = NULL) {
        if($request->has('first_name')){
            $data   =   $request->all();
            if ($request->hasFile('physician_image')) {
                $file            =  $request->file('physician_image');
                $destinationPath =  base_path() . '/public/physicians_imgs/';
                $filename        =  $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $data['image']   =  $filename;
            }
            unset($data['_token'],$data['physician_image']);
            $clinic  =   Physicians::find($id);
            $clinic->update($data);
            return redirect('physician/update/'.$id)->with('message', 'Contact sucessfully added');
        }
        $data = array(
            "page_title"    =>  "Edit ".$this->module,
            "page_heading"  =>  "Edit ".$this->module,
            "module"        =>  $this->module,
            "breadcrumbs"   =>  array("dashboard" => "Home", url('physicians') =>  ucfirst($this->plural)." List" , '#' =>'Edit '.ucfirst($this->module)),
        );
        $data['physician']  =   Physicians::find($id)->toArray();
        return view($this->view.'.edit-view', $data);
    }

    public function delete($id) {
        $physician   =  Physicians::find($id);
        $physician->delete();
        $response = array('flag' => true, 'msg' => $this->module . ' has been Deactivated');
        echo json_encode($response);
    }
}
