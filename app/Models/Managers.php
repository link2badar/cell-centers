<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Managers extends Model
{
    protected $table 		=	'managers';
	protected $primaryKey 	=	'manager_id';
	public    $timestamps   =   false;
	protected $fillable 	=	['first_name','last_name','email','phone_no','cell_no','address','city','state','date_birth','gender','zipcode','image'];
}
