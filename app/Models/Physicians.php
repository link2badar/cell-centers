<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Physicians extends Model
{
    protected $table 		=	'physicians';
	protected $primaryKey 	=	'physician_id';
	public    $timestamps   =   false;
	protected $fillable 	=	['first_name','last_name','email','phone_no','cell_no','address','city','state','date_birth','specialty','gender','zipcode','image'];
}
