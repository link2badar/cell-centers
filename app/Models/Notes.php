<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notes extends Model
{
    protected $table 		=	'notes';
	protected $primaryKey 	=	'note_id';
	public    $timestamps   =   false;
	protected $fillable 	=	['note_seminar_id','note_pa_id','note_user_id','note_detail','note_type'];
}
