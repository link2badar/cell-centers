<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{
    protected $table 		=	'locations';
	protected $primaryKey 	=	'loc_id';
	public $timestamps 		=	false;
	protected $fillable 	=	['location_name','location_address','location_city','location_state','location_zipcode','location_phone_no','location_fax','image','hours_description','description','info_description'];
}
