<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Patients extends Model
{
    protected $table 		=	'patients';
	protected $primaryKey 	=	'pa_id';
	public $timestamps 		=	false;
	protected $fillable 	=	['first_name','last_name','email','haboutus','cell_phone_number','phone_number','address','city','state','zipcode','date_birth','gender','preferred_clinic','case_manager','note','primary_concern','attendance','primary_physician'];
}
