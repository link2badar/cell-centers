@include('header')
<br>
<div class="row">
    <div class="col-md-4">
        <select class="select2 form-control" name="seminar_filter"> 
            <option value="1">Current Seminars</option>
            <option value="2">Archived Seminars</option>
        </select>
    </div>
    <div class="col-md-4 col-md-offset-4">
        <div class="pull-right">
            <a  href="<?php echo url('/seminar/add'); ?>" class="btn btn-block green-jungle"><i class="fa fa-fw fa-plus"></i> Add New {{ $module }} </a>            
        </div>
    </div>
    <div class="col-md-12" style="margin-top:8px">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-search"></i>Search {{ $module}} </div>
                <div class="tools">
                    <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('seminars')}}" method="get">
                    <div class="form-body">
                        <section class="row">
                            <div class="form-group col-md-4">
                                <label for="branch_id" class=" control-label">Date</label>
                                <div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                    <input type="text" name="start_date" class="form-control" placeholder="Start Date" data-provide="datepicker-inline" data-date-format="yyyy-mm-dd" data-date-autoclose="true">
                                    <span class="input-group-addon"> to </span>
                                    <input type="text" name="end_date" class="form-control" placeholder="End Date" data-provide="datepicker-inline" data-date-format="yyyy-mm-dd" data-date-autoclose="true">
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label" for="">City</label>
                                <select class="select2 form-control" name="city">
                                    <option value="">Select City</option>
                                    @if(!empty($cities))
                                        @foreach($cities as $key => $city)
                                            <option>{{ $city['city'] }}</option>
                                        @endforeach 
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label" for="">State</label>
                                <select class="select2 form-control" name="state">
                                    <option value="">Select State</option>
                                    @if(!empty($states))
                                        @foreach($states as $key => $state)
                                            <option>{{ $state['state'] }}</option>
                                        @endforeach 
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label" for="">Zip Code</label>
                                <input type="text" name="zip_code" class="form-control" placeholder="Zip Code">
                            </div>
                        </section>
                    </div>
                    <div class="form-actions ">
                        <button type="submit" class="btn blue pull-right"><i class="fa fa-search"></i> Search</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>  
                                <th class="text-center"> #          </th>
                                <th class="text-center"> Date       </th>
                                <th class="text-center"> Title      </th>
                                <th class="text-center"> Location   </th>
                                <th class="text-center"> Time       </th>
                                <th class="text-center"> Private    </th>
                                <th class="text-center"> Detail     </th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($seminars))
                                @foreach($seminars as $key => $li)
                                    <tr class="text-center list_{{++$key}} list">
                                        <td>
                                            {{ $key }} 
                                        </td>
                                        <td>
                                            {!! ($li['date']) ? $li['date'] : '<span class="badge badge-danger"> N/A </span>' !!}
                                        </td>
                                        <td>
                                            {!! ($li['title']) ? $li['title'] : '<span class="badge badge-danger"> N/A </span>' !!}
                                        </td>
                                        <td>
                                            {!! ($li['address']) ? $li['address'] : '<span class="badge badge-danger"> N/A </span>' !!}
                                        </td>
                                        <td>
                                            {!! ($li['start_time']) ? date('h:i A',$li['start_time']) : '<span class="badge badge-danger"> N/A </span>' !!}
                                        </td>
                                        <td>
                                            <?php $private = [0=>'',1=>"Yes",2=>'No'] ?>
                                            {!! ($li['private']) ? $private[$li['private']] : '<span class="badge badge-danger"> N/A </span>' !!}
                                        </td>
                                        <td>
                                            <a href="{{ url('/seminar/detail/'.$li['seminar_id']) }}"><i class="fa fa-detail"></i>Detail</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')