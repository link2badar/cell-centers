@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="pull-right">
            <a class="btn yellow-gold"  href="{{ url('/seminar/update/'.$seminar['seminar_id']) }}">
                Edit {{ $module }}
            </a>
            <a class="btn green-meadow" data-toggle="modal" href="#data_modal" onclick="loadModal('seminar/admin-modal',{{ $seminar['seminar_id'] }})">
                <i class="fa fa-plus"></i> Add {{ $module }} Admin
            </a>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <?php echo "Seminar | ".$seminar['title']." ".$seminar['start_time']; ?>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div><!-- portlet-collapsed -->
            <div class="portlet-body">
            	<div class="row">
            		<div class="col-md-4">
            			<table class="table table-hover">
		            		<tr>
		            			<th>Location:</th>
		            			<td>{!! $seminar['address']." ".$seminar['city']." ".$seminar['state']." ".$seminar['zip_code']." ".$seminar['country'] !!}</td>
		            		</tr>
		            		<tr>
		            			<th>Registrants:</th>
		            			<td>{{ count($registrants) }}</td>
		            		</tr>
		            		<tr>
		            			<th>Attendees:</th>
		            			<td>0</td>
		            		</tr>
		            		<tr>
		            			<th>Exams:</th>
		            			<td>0</td>
		            		</tr>
		            	</table>
            		</div>
            		<div class="col-md-4">
            			<table class="table table-hover">
		            		<tr>
		            			<th>Date:</th>
		            			<td>{{ $seminar['date'] }}</td>
		            		</tr>
		            		<tr>
		            			<th>Time:</th>
		            			<td>{{ date('h:i A',$seminar['start_time']) }} - {{ date('h:i A',$seminar['end_time']) }}</td>
		            		</tr>
		            		<tr>
		            			<th>Private:</th>
		            			<td>
                                    <?php $private = [0=>'',1=>"Yes",2=>'No'] ?>
                                    {!! ($seminar['private']) ? $private[$seminar['private']] : '<span class="badge badge-danger"> N/A </span>' !!}               
                                </td>
		            		</tr>
		            	</table>
            		</div>
            		<div class="col-md-4">
            			<table class="table list">
		            		<tr>
		            			<th>Admins:</th>
		            			<td></td>
		            		</tr>
		            		@if(!empty($seminar_admins))
		            			<form action="{{ url('seminar/update-admin') }}" method="post" class="make_ajax">
		            			<input type="hidden" name="admin" value="1">
                                <input type="hidden" name="seminar_id" value="{{$seminar['seminar_id']}}">
		            			{{ csrf_field() }}
		            			@foreach($seminar_admins as $seminar_admin)
				            		<tr class="list_{{$seminar_admin['seminar_admin_id']}}">
				            			<th>
                                            <div class="md-radio">
                                                <input type="radio" id="{{$seminar_admin['user']}}-radio" name="user" class="md-radiobtn" value="{{ $seminar_admin['seminar_admin_id'].'-'.$seminar_admin['user']}}" <?php if($seminar_admin['admin'] == 1) echo 'checked="checked"' ?>>
                                                <label for="{{$seminar_admin['user']}}-radio">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                </label>
                                            </div>
                                        </th>
				            			<td>{{ $seminar_admin['first_name'].' '.$seminar_admin['last_name'] }}  {!! ($seminar_admin['admin'] == 1) ? '<span class="bold"> - Presenter</span>' : ''; !!}</td>
				            			<td>
				            				<a class="delete font-red-thunderbird" data-url="{{url('seminar/delete-admin')}}/{{ $seminar_admin['seminar_admin_id'] }}" href="javascript:void(0);" data-remove="list_{{$seminar_admin['seminar_admin_id']}}"><i class="fa fa-times"></i></a>
				            			</td>
				            		</tr>
				            	@endforeach
				            	<tr>
				            		<th><button class="btn green-jungle" type="submit" name="submit">Update Presenter</button></th>
				            	</tr>
				            	</form>
		            		@endif
		            	</table>
	            	</div>
            	</div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
					Lead Source Totals
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body portlet-collapsed">
            	<div class="row">
            		<div class="col-md-12">
                        <?php $lead_sources = config('constants.haboutus') ?>
                        @if(!empty($lead_sources))
            			<table class="table">
                            @foreach($lead_sources as $source_key => $source)
            				<tr>
            					<th>{{ $source }}</th>
            					<td>0</td>
            				</tr>
                            @endforeach
            			</table>
                        @endif
            		</div>
            	</div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="pull-right">
            <a class="btn green-meadow" href="{{ url('seminar/add-registrant').'/'.$seminar['seminar_id'] }} ">
                <i class="fa fa-plus"></i> Add {{ $module }} Registrant
            </a>
        </div>
    </div>
    <div class="col-md-12" style="margin-top:10px;">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
					Registrants ({{ count($registrants )}})
                </div>
                <div class="tools">
                   <a href="javascript:;" class="collapse" data-original-title="" title=""></a> 
                </div>
            </div>
            <div class="portlet-body">
                 <div class="table-responsive">
                    <table class="table table-bordered list">
                        <thead>
                            <tr>  
                                <th class="text-center"> #                  </th>
                                <th class="text-center"> Name               </th>
                                <th class="text-center"> Registration Date  </th>
                                <th class="text-center"> Pain Area          </th>
                                <th class="text-center"> Attendance         </th>
                                <th class="text-center"> Exam?              </th>
                                <th class="text-center"> Edit               </th>
                                <th class="text-center"> Cancel             </th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($registrants))
                                @foreach($registrants as $key => $li)
                                    <tr class="text-center list_{{++$key}} list">
                                        <td>
                                            {{ $key }}
                                        </td>
                                        <td>
                                            {!! ($li['first_name']) ? $li['first_name']." ".$li['last_name'] : '<span class="badge badge-danger"> N/A </span>' !!}
                                        </td>
                                        <td>
                                            {{ date('m/d/Y h:i A', strtotime($li['created_at'])) }}
                                        </td>
                                        <td>
                                            <?php
                                                $primary_concerns = config('constants.primary_concerns');
                                                if($li['primary_concern'] != 0):
                                                    echo $primary_concerns[$li['primary_concern']];
                                                else:
                                                    echo "No conditions are associated";
                                                endif;
                                            ?>
                                        </td>
                                        <td>
                                            <select class="select2" style="width:100%" name="registrant_attendance">
                                            <?php
                                                $attendance =  [0=>'Not Presen',1=>'Present'];
                                                foreach ($attendance as $key => $value) {
                                                    $sel = ($key == $li['attendance']) ? 'selected="selected"' : '';
                                                    echo '<option '.$sel.' value="'.$key.'-'.$li['pa_id'].'">'.$value.'</option>';
                                                }                                               
                                            ?>
                                            </select>
                                        </td>
                                        <td>
                                            No
                                        </td>
                                        <td>
                                            <a href="{{ url('/seminar/edit-registrant/'.$li['pa_id'])}}"><i class="fa fa-pencil-square-o font-green-jungle"></i></a>
                                        </td>
                                        <td>
                                            <a class="delete font-red-thunderbird" data-url="{{ url('/seminar/delete-registrant/'.$li['pa_id'])}}" href="javascript:void(0);" data-remove="list_{{$key}}"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')