@include('header')
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/contact/add')}}" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="form-body row">
                    
                    <div class="col-md-6">
                      <label class="control-label">First Name* :</label>
                        <input type="text" class="form-control" required="required" id="first_name" name="first_name" >
                    </div>
                    <div class="col-md-6">
                      <label class="control-label">Last Name*:</label>
                        <input type="text" class="form-control" required="required" id="last_name" name="last_name" >
                    </div>
                    <div class="col-md-6">
                      <label class="control-label">Email Address*</label>
                        <input type="text" class="form-control" required="required"  name="email" >
                    </div>
                    
                    <div class="col-md-6">
                      <label class="control-label">Phone Number </label>
                        <input type="text" class="form-control" name="phone_number" >
                    </div>
                 
                    <div class="col-md-12">
                      <label class="control-label">Address </label>
                        <input type="text" class="form-control" name="address" >
                    </div>
                    <div class="col-md-6">
                      <label class="control-label">City </label>
                        <input type="text" class="form-control" name="city" >
                    </div>
                    <div class="col-md-3">
                      <label class="control-label">State </label>
                        <input type="text" class="form-control" name="state" >
                    </div>
                    <div class="col-md-3">
                      <label class="control-label">Zip/Postal Code  </label>
                        <input type="text" class="form-control" name="zipcode" >
                    </div>
                   <div class="col-md-6">
                      <label class="control-label">How Did You Hear About Us?* </label>
                        <select type="text" class="form-control select2" required="required" name="haboutus">
                          <option value="">Choose How Did You Hear About Us</option>
                          <?php 
                          $arr = config('constants.haboutus');
                          foreach ($arr as $key=>$val) {
                            echo '<option value="'.$key.'">'.$val.'</option>';
                          }
                          ?>
                          
                        </select>
                    </div>
                    <div class="col-md-12">
                      <label class="control-label">Message:  </label>
                        <textarea  class="form-control" name="Message" id="summernote_1" ></textarea>
                    </div>
                    
                  </div>
                  
                  <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                </form>
            </div>
        </div>
    </div>
    @include('footer')
</div>