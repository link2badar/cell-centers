@include('header')

<div class="row">
  <div class="col-md-2 pull-right">
    <br/>
    <a  href="<?php echo url('/contact/add'); ?>" class="btn btn-block btn-info"><i class="fa fa-fw fa-plus"></i> Add Contact</a>
    <br/>
  </div>
</div>
<div class="row">

    <div class="col-md-12">

        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
                
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th> Name </th>
           
                        <th> Email </th>
                        <th> Phone </th>
                        <th> Edit </th>

                      </tr>
                    </thead>
                    <tbody>
                     
                        
                         <?php 
                            $i  = 1;
                            $html = "";

                            if(count($list['data']) > 0){
                              foreach($list['data'] as $key=>$row)  { 
                                $html .= '<tr class="row-'.$row['con_id'].'">';
            
                                $html .= '<td>'.$row['first_name']." ".$row['last_name'].'</td>';
                                $html .= '<td>'.$row['email'].'</td>';
                                $html .= '<td>'.$row['phone_number'].'</td>';
                                $html .= '<td><a class="btn btn-xs blue" href="'.url('/contact/update/'.$row['con_id']).'"><i class="fa fa-edit"></i></a></td>';
                                $html .= '</tr>';
                              }
                              echo $html;
                            }
                            else {
                              echo '<tr><td colspan="6" align="center"><h4>You have not any Stores. to add <a href="'.url('/').'/'.$module.'/rss/add">click here</a></h4></td><tr>'; 
                            }
                            ?>
                    </tbody>
                  </table>
                    
                </div>
            </div>
        </div>
    </div>
    @include('footer')
</div>