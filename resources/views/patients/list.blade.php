@include('header')
<br>
<div class="row">
    <div class="col-md-2 pull-right">
        <a  href="<?php echo url('/patient/add'); ?>" class="btn btn-block btn-info"><i class="fa fa-plus"></i> Add Patient</a>
        <br/>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th> Name </th>
                                <th> Location </th>
                                <th> DOB </th>
                                <th> Primary Concern(s) </th>
                                <th> Edit </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $i      =   1;
                                $html   =   "";
                                if(count($list['data']) > 0){
                                    foreach($list['data'] as $key=>$row)  { 
                                        $html .= '<tr class="row-'.$row['pa_id'].' list_'.++$key.' list">';
                                        $html .= '<td>'.$row['first_name']." ".$row['last_name'].'</td>';
                                        $html .= '<td>'.$row['state'].' '.$row['city'].'</td>';
                                        if(!empty($row['date_birth'])){
                                            $html .= '<td>'.$row['date_birth'].'</td>';
                                        }else{
                                            $html .= '<td>No associated</td>';
                                        }
                                        if(!empty($row['primary_concern'])){
                                            $primary_concerns   =   explode(',',$row['primary_concern']);
                                            $arr                =   config('constants.primary_concerns');
                                            $concerns           =   '';
                                            foreach ($primary_concerns as $concern){
                                                $concerns .= $arr[$concern]."<br>";  
                                            }
                                            $html .= '<td>'.$concerns.'</td>';
                                        }else{
                                            $html .= '<td>No associated conditions</td>';
                                        }
                                        $html .= '<td><a class="btn btn-xs blue" href="'.url('/patient/update/'.$row['pa_id']).'"><i class="fa fa-edit"></i></a><a class="delete btn btn-xs red" data-url="'.url('/patient/delete/').'/'.$row['pa_id'].'" href="javascript:void(0);" data-remove="list_'.$key.'"><i class="fa fa-trash"></i></a></td>';
                                        $html .= '</tr>';
                                    }
                                    echo $html;
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('footer')
</div>