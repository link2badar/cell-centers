<!DOCTYPE html>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>{{$page_title}}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for statistics, charts, recent events and reports" name="description" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ url('/')}}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/')}}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/')}}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/')}}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{ url('/')}}/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/')}}/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/')}}/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/')}}/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/')}}/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ url('/')}}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ url('/')}}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ url('/')}}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/')}}/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{ url('/')}}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 

        <link href="{{ asset('/') }}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/') }}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/') }}/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/') }}/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/') }}/assets/global/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet" type="text/css" />

        <!-- NOTIFICATION CSS -->
        <link href="{{ asset('/') }}/assets/global/plugins/jquery-notific8/jquery.notific8.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/') }}/assets/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <!--  Image Viewer -->
        <link href="{{ asset('/') }}/assets/image-viewer/imageviewer.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            #iv-container {
                position: fixed;
                background: #0d0d0d;
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                display: none;
                z-index: 20000;
            }
            /* Select2 hide selected option */
            .select2-results__option[aria-selected=true] {
                display: none;
            }
        </style>
        <script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
        <script>
            tinymce.init({ selector: '.tinymce' });
        </script>
    </head>
    <!-- END HEAD -->
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="index.html">
                            <img src="{{ url('/')}}/assets/layouts/layout/img/logo.png" alt="logo" class="logo-default" /> </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="{{ url('/')}}/assets/layouts/layout/img/avatar3_small.jpg" />
                                    <span class="username username-hide-on-mobile"> {{ Auth::user()->name }} </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="{{ url('/logout')}}">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            <li class="nav-item start active">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                    <!-- <span class="selected"></span>
                                    <span class="arrow open"></span> -->
                                </a>
                            </li>
                            <li class="nav-item start">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Admin Stuff</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu" style="display: none;">
                                    <li class="nav-item start ">
                                        <a href="{{ url('/clinics') }}" class="nav-link ">
                                            <i class="fa fa-hospital-o"></i>
                                            <span class="title">Preferred Clinics</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="{{ url('/managers') }}" class="nav-link ">
                                            <i class="fa fa-user"></i>
                                            <span class="title">Case Managers</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="{{ url('/physicians') }}" class="nav-link ">
                                            <i class="fa fa-users"></i>
                                            <span class="title">Primary Physicians</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="{{ url('/seminar-users') }}" class="nav-link ">
                                            <i class="fa fa-users"></i>
                                            <span class="title">Seminar Users</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item start">
                                <a href="{{url('patients')}}" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Patients</span>
                                </a>
                            </li>
                            <li class="nav-item start">
                                <a href="{{ url('/calendar')}}" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Calendar</span>
                                </a>
                            </li>
                            <li class="nav-item start">
                                <a href="{{url('contacts')}}" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Contact Queue</span>
                                </a>
                            </li>
                            <li class="nav-item start">
                                <a href="{{ url('/seminars') }}" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Seminars</span>
                                </a>
                            </li>
                            <li class="nav-item start">
                                <a href="{{ url('/locations')}}" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Locations</span>
                                </a>
                            </li>
                            <li class="nav-item start">
                                <a href="{{ url('/staff') }}" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Staff</span>
                                </a>
                            </li>
                            <li class="nav-item start">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Templates</span>
                                </a>
                            </li>
                            <!-- <li class="heading">
                                <h3 class="uppercase">Features</h3>
                            </li> -->
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                        <div class="theme-panel hidden-xs hidden-sm">
                        </div>
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar" >
                            <ul class="page-breadcrumb">
                            @foreach ($breadcrumbs as $key => $value)
                                <li>
                                @if($key != "#")
                                    <a href="{{url($key)}}">{{$value}}</a>
                                    <i class="fa fa-circle"></i>
                                @else
                                    <span>{{$value}}</span>
                                @endif
                                </li>
                            @endforeach
                            </ul>
                            <div class="page-toolbar">
                                @yield('content')
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                             