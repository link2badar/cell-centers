@include('header')
<br>
<div class="row">
    <div class="col-md-12">
    	<span class="pull-right">
			<a href="javascript:;" class="btn green-jungle"><i class="fa fa-plus"></i> Add New Patient</a>
			<a href="{{ url('appointment/add')}}" class="btn green-jungle"><i class="fa fa-plus"></i> Add New Appointment</a>
		</span>
    </div>
    <div class="col-md-12" style="margin-top:12px;">
        <div class="portlet light portlet-fit bordered calendar">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">Calendar</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <!-- BEGIN DRAGGABLE EVENTS PORTLET-->
                        <h3 class="event-form-title margin-bottom-20">Draggable Events</h3>
                        <div id="external-events">
                            <form class="inline-form">
                                <input type="text" value="" class="form-control" placeholder="Event Title..." id="event_title" />
                                <br/>
                                <a href="javascript:;" id="event_add" class="btn green"> Add Event </a>
                            </form>
                            <hr/>
                            <div id="event_box" class="margin-bottom-10"></div>
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" for="drop-remove"> remove after drop
                                <input type="checkbox" class="group-checkable" id="drop-remove" />
                                <span></span>
                            </label>
                            <hr class="visible-xs" /> </div>
                        <!-- END DRAGGABLE EVENTS PORTLET-->
                    </div>
                    <div class="col-md-9 col-sm-12">
                        <div id="calendar" class="has-toolbar"> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')