@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cogs"></i><?php echo isset($page_heading)?$page_heading:""; ?></div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="{{url('/appointment/add')}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body row">
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="doctor">Doctor</label>
                            <select class="select2" name="doctor" style="width:100%" id="doctor">
                                <option value="">Choose A Doctor</option>
                                @if(!empty($physicians))
                                    @foreach($physicians as $physician)
                                        <option value="{{ $physician['physician_id'] }}">{{ $physician['first_name'].' '.$physician['last_name'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="patient">Patient</label>
                            <select class="select2" name="patient" style="width:100%" id="patient">
                                <option value="">Choose A Patient</option>
                                @if(!empty($patients))
                                    @foreach($patients as $patient)
                                        <option value="{{ $patient['pa_id'] }}">{{ $patient['first_name'].' '.$patient['last_name'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                         <div class="col-md-6 form-group">
                            <label class="control-label" for="doctor">Appointment Type </label>
                            <select class="select2" name="doctor" style="width:100%" id="doctor">
                                <option value="">Choose A Appointment Type</option>
                                <?php $appointment_types = config('constants.appointment_types'); ?>
                                @if(!empty($appointment_types))
                                    @foreach($appointment_types as $key => $appointment_type)
                                        <option value="{{ $key }}">{{ $appointment_type }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="date">Date (MM/DD/YYYY)</label>
                            <div class="input-group">
                                <input  type="text" class="form-control required datepicker" placeholder=" (MM/DD/YYYY) " name="date" id="date">
                                <span class="input-group-addon bg-blue bg-font-blue">
                                    <i class="fa fa-calendar-plus-o"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="patient">Available Start Time (* indicates booked times) </label>
                            <select class="select2" name="patient" style="width:100%" id="patient">
                                <option value="">Choose An Available Start Time</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="doctor">Available End Time  </label>
                            <select class="select2" name="doctor" style="width:100%" id="doctor">
                                <option value="">Choose An End Time</option>
                            </select>
                        </div>
                        <div class="col-md-12 form-group">
                            <label class="control-label">Primary Concern(s)</label>
                            <select type="text" class="form-control select2" name="primary_concern">
                                <?php 
                                    $arr = config('constants.primary_concerns');
                                    foreach ($arr as $key=>$val) {
                                        echo '<option value="'.$key.'">'.$val.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <span class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Save {{ $module}}</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')