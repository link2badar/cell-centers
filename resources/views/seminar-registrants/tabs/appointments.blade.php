@extends('seminar-registrants.tabs.tabs-header')
@section('tab-content')
<div class="tab-pane active" id="tab_1">
	<div class="row">
		<div class="col-md-12">
			<h3 class="bold">Upcoming Scheduled Appointments</h3>
			<p class="bold">No upcoming appointments.</p>
		</div>
		<div class="col-md-12">
			<h3 class="bold">Past Scheduled Appointments</h3>
			<p class="bold">No past appointments.</p>
		</div>
	</div>
</div>
@endsection