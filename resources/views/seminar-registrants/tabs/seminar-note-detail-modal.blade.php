<div class="modal-content">
    <div class="modal-header bg-blue bg-font-blue">
        <h5 class="modal-title" id="exampleModalLabel">
            <b>Full Note</b>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                ×
            </span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12 form-group">
                <p>{{ $note['note_detail'] }}</p>
            </div>
        </div>
    </div>
</div>