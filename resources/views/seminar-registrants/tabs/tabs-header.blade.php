@include('header')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Styled Tabs #2 </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tabbable-custom">
                    <ul class="nav nav-tabs">
                        <li class="{{  ($current_tab == 'patient_info')? 'active' : '' }}">
                            <a href="{{ url('seminar/edit-registrant/'.$registrant_id) }}" data-toggle="" aria-expanded="false"> Patient Info </a>
                        </li>
                        <li class="{{  ($current_tab == 'log')? 'active' : '' }}">
                            <a href="{{ url('/patient/log/'.$registrant_id)}}" data-toggle="" aria-expanded="false"> Log </a>
                        </li>
                        <li class="{{  ($current_tab == 'appointments')? 'active' : '' }}">
                            <a href="{{ url('/patient/appointments/'.$registrant_id)}}" data-toggle="" aria-expanded="true"> Appointments </a>
                        </li>
                        <li class="{{  ($current_tab == 'invoices')? 'active' : '' }}">
                            <a href="{{ url('/patient/invoices/'.$registrant_id)}}" data-toggle="" aria-expanded="true"> Invoices </a>
                        </li>
                        <li class="{{  ($current_tab == 'seminars')? 'active' : '' }}">
                            <a href="{{ url('/patient/seminars/'.$registrant_id)}}" data-toggle="" aria-expanded="true"> Seminars </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        @yield('tab-content')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')