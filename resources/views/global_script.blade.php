<script type="text/javascript">    
    var site_url = "{{url('/')}}";

    $(document).on("click", ".image-viewer", function(event) {
        var viewer = ImageViewer();
        viewer.show($(this).attr('src'));
    });

    $(document).on("change","select[name='seminar_filter']",function(){
        console.log($(this).val());
    });

    $(document).ready(function() {
        /* Init Select2 and hide selected option */
        $('.select2').select2();
        $("select").on("select2:select", function (evt) {
            var element = evt.params.data.element;
            var $element = $(element);
            $element.detach();
            $(this).append($element);
            $(this).trigger("change");
        });
        $("select[name='seminar_admin_ids[]']").select2({
            maximumSelectionLength: 4
        });

        $('.datepicker').datepicker({
            todayHighlight:  true,
            format: "yyyy/mm/dd",
            autoclose: true,
        });
        
        $('.timepicker').timepicker();

        $('.list thead input[type="checkbox"]').click(function(event) { //on click 
            if (this.checked) { // check select status
                $('.list tbody input[type="checkbox"]').each(function() { //loop through each checkbox
                    this.checked = true; //select all checkboxes with class "checkbox1"               
                });
            } else {
                $('.list tbody input[type="checkbox"]').each(function() { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                });
            }
        });

        $('.singcheck input[type="checkbox"]').click(function(event) { //on click 
            if (this.checked) { // check select status
                $('.mulcheck input[type="checkbox"]').each(function() { //loop through each checkbox
                    this.checked = true; //select all checkboxes with class "checkbox1"               
                });
            } else {
                $('.mulcheck input[type="checkbox"]').each(function() { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                });
            }
        });
       
        /* MAKE AJAX CALL */
        $(document).on("submit", "form.make_ajax", function(event) {
            event.preventDefault();
            var form    =   $(this).serialize();
            var btn     =   "form.make_ajax button[type=submit]";
            var btntxt  =   $(btn).html();
            type        =   $(this).attr("method");
            addWait(btn, "working...");
            $.ajax({
                type:       $(this).attr("method"),
                cache:      false,
                data:       form,
                url:        $(this).attr("action"),
                dataType:   "json",
                type:       type,
                headers:    {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(res) {
                    removeWait(btn, btntxt);
                    toastr["success"](res.msg, "Completed!");
                    if(typeof(res.reload) !== 'undefined' && res.reload == true){
                        location.reload();
                    }
                }
            });
            return false;
        });

        $('select[name="registrant_attendance"]').on('change',function(){
            $.ajax({
                type:       "POST",
                cache:      false,
                data:       {param:$(this).val()},
                url:        site_url+"/seminar/registrant-attendance",
                dataType:   "json",
                headers:    {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(res) {
                    removeWait(btn, btntxt);
                    toastr["success"](res.msg, "Completed!");
                }
            });
        });
        $('.preferredClinics').on('change',function(){
            $.ajax({
                type:       "POST",
                cache:      false,
                data:       {param:$(this).val()},
                url:        site_url+"/patient/filter-provider",
                dataType:   "json",
                headers:    {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(res) {
                    filteredResult  = ''; filteredPhysicianResult ='';
                    for(var i = 0; i < res.manager.length; i++) {
                        filteredResult += '<option value="'+res.manager[i].id+'">'+res.manager[i].name+'</option>';
                    }
                    for (var i = 0; i < res.physician.length; i++) {
                        filteredPhysicianResult += '<option value="'+res.physician[i].id+'">'+res.physician[i].name+'</option>';
                    }
                    $('select[name="case_manager"]').html(filteredResult);
                    $('select[name="primary_physician"]').html(filteredPhysicianResult);
                }

            });
        });
    });

    function loadModal(url, param) {
        $("#data_modal .modal-content").html('<p style="text-align: center;"><i class="fa fa-spinner fa-spin"></i>  Please wait loading...</p>');
        if (typeof(param) === 'undefined') param = null;
        url = site_url +'/'+ url + "?param=" + param;
        console.log(url);
        $.ajax({
            type: "GET",
            cache: false,
            url: url,
            success: function(result) {
                $("#data_modal .modal-content").html(result);
                $('.select2').select2({});
                tinymce.init({ selector: '#mytextarea' });
            }
        });
    }

    function addWait(dom, lable) {
        $(dom).attr("disabled", "disabled");
        string = '<i class="fa fa-spinner fa-spin"></i> ' + lable;
        $(dom).html(string);
    }

    function removeWait(dom, lable) {
        $(dom).removeAttr("disabled");
        $(dom).html(lable);
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.upload-image-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".upload-image").change(function() {
        readURL(this);
    });

    $(document).on("click", ".list .delete", function(event) {
        url      = $(this).attr("data-url");
        remvove  = $(this).attr("data-remove");
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this record!",
                type: "warning",
                showCancelButton:   true,
                confirmButtonClass: "btn-danger",
                confirmButtonText:  "Yes, delete it!",
                cancelButtonText:   "No, cancel !",
                closeOnConfirm:     false,
                closeOnCancel:      false
        },
        function(isConfirm) {
            if(isConfirm) {
                $.ajax({
                    type:  "GET",
                    cache: false,
                    url:   url,
                    dataType: "json",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(res) {
                        if(res.flag == true) {
                            swal("Deleted!", "Your record has been deleted.", "success");
                            $("." + remvove).remove();
                            if(typeof(res.reload) !== 'undefined' && res.reload == true){
                                location.reload();
                            }
                        }
                    }
                });
        }else{
            swal("Cancelled", "Your imaginary file is safe :)", "error");
            }
        });
    });
</script>
@if(session()->has('message'))
<script type="text/javascript">toastr["success"]('{{ session()->get('message') }}',"Completed!" );</script>
@endif
@if(session()->has('danger_message'))
<script type="text/javascript">toastr["error"]('{{ session()->get('danger_message') }}',"Not Uploaded!" );</script>
@endif
@if(session()->has('warning-message'))
<script type="text/javascript">toastr["warning"]('{{ session()->get('warning-message') }}',"Oops!" );</script>
@endif