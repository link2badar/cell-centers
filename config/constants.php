<?php
return  [ 
	'haboutus'=>
				[
					'TV',
					'News Print',
					'Facebook',
					'Google',
					'Online Advertisement',
					'Magazine Advertisement',
					'Personal Referral',
					'Billboard',
					'Direct Mail',
					'Radio',
				],
	'gender'	=>
				[
					1=>'Male',
					2=>'Female',
				],
	'PreferredClinic'	=>
						[
							1=>'CDA Spokane',
						],
	'CaseManager'=>
	[
		1=>'Jhone Smith',
	],
	'PrimaryConcern'	=>	[
								1=>'Jhone Smith',
							],

	'positions'			=>	[
								1	=>	'Provider',
								2	=>	'Case Manager',
								3	=>	'Location User',
								4	=>	'Location Seminar User',
							],

	'countries' 	=>	[
							1	=> "USA",
							2	=> "Canada"	
						],

	'primary_concerns' 	=>	[
						1 	=>  	'Ankle Pain',
                        2 	=>   	'Arm Pain',
                        3 	=>   	'Elbow Pain',
                        4 	=>   	'Foot/Toe Pain',
                        5 	=>   	'Hand/Finger Pain',
                        6 	=>  	'Head/Facial Pain',
                        7 	=>   	'Hip Pain',
                        8 	=>   	'Knee Pain',
                        9 	=>   	'Leg Pain',
                        10 	=>   	'Lung Disease',
                        11 	=>   	'Other Pain Syndromes - Torso/Chest Wall/Abdominal Wall/Myofascial',
                        12 	=>   	'Peripheral Neuropathy',
                        13 	=>   	'Shoulder Pain',
                        14	=>   	'Spine Pain',
                        15 	=>  	'Wrist Pain',

	],

	'appointment_types'	=>	[
						1 	=>  	'Phone Consultation',
                        2 	=>   	'Consultation',
                        3 	=>   	'Exam',
                        4 	=>   	'Re-Exam',
                        5 	=>   	'Treatment',
                        6 	=>  	'Re-Treatment',
	],
];